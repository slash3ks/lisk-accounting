#!/usr/bin/env python

import re
import sys
import json
import math
import time
import getpass
import logging
import requests
import datetime
import argparse
import pylisk
try:
    import psycopg2
except ImportError:
    msg = 'Please install psycopg2. sudo apt install python3-psycopg2'
    sys.exit(msg)

LOG = logging.getLogger(__name__)

def name_change(api, delegate):

    # if not an address then it is a name
    # try to convert to an address
    if not re.search(r'^\d+L$', delegate):

        payload = {'parameters':'?username={}'.format(delegate)}
        delegate_info = api.delegates('delegate_list',payload)

        try:
            return delegate_info['data'][0]['account']['address']
        except IndexError:
            return None

    else:

        return delegate

def static_name_mapping():
    """

    """
    try:
        with open('./name_map.json') as nmfh:
            name_map = json.load(nmfh)
            return name_map
    except IOError:
        sys.exit('Could not open name_map.json')

def db_name_mapping(conn):
    """

    """
    sql_query = '''
        SELECT *
        FROM mem_accounts AS m
        '''
    sql_query_args = {}
    cursor = conn.cursor()
    cursor.execute(sql_query, sql_query_args)
    column_names = [desc[0] for desc in cursor.description]

    results = []
    for row in cursor:
        try:
            new_row = [x.tobytes().hex() if isinstance(x, memoryview) else x for x in row]
            results.append(dict(zip(column_names, new_row)))
        except:
            raise

    return results


def db_exec(conn, sql_query, sql_query_args):
    """

    """
    sql_query = '''
        SELECT *
        FROM mem_accounts AS m
        '''
    sql_query_args = {}
    cursor = conn.cursor()
    cursor.execute(sql_query, sql_query_args)
    column_names = [desc[0] for desc in cursor.description]

    results = []
    for row in cursor:
        try:
            new_row = [x.tobytes().hex() if isinstance(x, memoryview) else x for x in row]
            results.append(dict(zip(column_names, new_row)))
        except:
            raise

    return results

def get_config(stage, user_nfo):
    """
    """
    configuration_items = {
        'db': {
            'database': user_nfo['database'],
            'user': user_nfo['user'],
            'host': 'localhost',
            'password': user_nfo['password']
        }
    }

    if stage == 'main':
        configuration_items['db']['database'] = 'lisk_main'

    connect_str = "dbname='{database}' user='{user}' host='{host}' password='{password}'"
    conn = psycopg2.connect(connect_str.format(**configuration_items['db']))

    return conn


def get_url(stage, dpos):
    """
    """
    url = 'http://localhost:'

    if stage == 'main' and dpos == 'lisk':
        url += '8000'
        user_nfo = {
                'user': getpass.getuser(),
                'database': 'lisk_main',
                'password': 'password'}
    elif stage == 'test' and dpos == 'lisk':
        url += '7000'
        user_nfo = {
                'user': getpass.getuser(),
                'database': 'lisk_test',
                'password': 'password'}

    return url, user_nfo

def get_transactions(conn, account, received, sent, timestamp):
    """
    """
    if received and sent:
        where_clause = '(t."t_senderId" = %(sender)s OR t."t_recipientId" = %(recp)s)'
    elif received and not sent:
        where_clause = 't."t_recipientId" = %(recp)s'
    elif sent and not received:
        where_clause = 't."t_senderId" = %(sender)s'

    sql_query = '''
        SELECT *
        FROM trs_list AS t
        WHERE {} AND
        t."t_timestamp" >= %(fromt)s
        ORDER by t."b_height" DESC
        '''.format(where_clause)
    
    sql_query_args = {'recp': account, 'sender': account}
    sql_query_args['fromt'] = timestamp - 1464109200
    
    cursor = conn.cursor()
    cursor.execute(sql_query, sql_query_args)
    
    column_names = [desc[0] for desc in cursor.description]
    
    results = []
    for row in cursor:
        try:
            new_row = [x.tobytes().hex() if isinstance(x, memoryview) else x for x in row]
            results.append(dict(zip(column_names, new_row)))
        except:
            raise

    return results

def tx_parse(conn, transactions, filter_date, exchange_data, tx_types, fname):
    """

    """
    txobj = {'data': []}
    #name_map = static_name_mapping()
    db_name_map = db_name_mapping(conn)

    transaction_types = {
        0: "Normal transaction",
        1: "2nd sig creation",
        2: "Delegate registration",
        3: "Delegate vote",
        4: "Multi-sig creation",
        5: "Dapp registration",
        6: "?",
        7: "Dapp deposit",
        8: "Dapp withdrawal"}

    transaction_fees = {
        0: 10000000,
        1: 500000000,
        2: 2500000000,
        3: 100000000,
        4: 500000000,
        5: 2500000000,
        6: 0,
        7: 0,
        8: 0}

    for tx in transactions:

        new_timestamp = tx['t_timestamp'] + 1464109200
        date = datetime.datetime.fromtimestamp(new_timestamp).strftime('%Y-%m-%d %H:%M:%S')
        day = datetime.datetime.fromtimestamp(new_timestamp).strftime('%Y-%m-%d')

        # Check if it exists
        if any(x['id'] == tx['t_id'] for x in txobj['data']):
            continue

        tx_amount = float(int(tx['t_amount']) / math.pow(10, 8))

        # Get the name of the sender
        #match = next((l for l in db_name_map if l['address'] == tx['t_senderId']), None)
        #from_delegate_name = match.get('username')
        from_delegate_name = tx['t_senderId']

        # Get the name of the recipient
        #match = next((l for l in db_name_map if l['address'] == tx['t_recipientId']), None)
        #to_delegate_name = match.get('username')
        to_delegate_name = tx['t_recipientId']

        try:
            day_price = exchange_data[day]
        except KeyError:
            day_price = {'high': 0, 'low': 0, 'close': 0}

        # Filter by tx type
        if tx['t_type'] not in tx_types:
            continue

        # Filter by delegate name
        if fname and fname not in from_delegate_name:
            continue

        # Append to tx object what is left
        txobj['data'].append({'fromAddr': tx['t_senderId'],
                              'fromName': from_delegate_name,
                              'toAddr': tx['t_recipientId'],
                              'toName': to_delegate_name,
                              'amount': tx_amount,
                              'fee': transaction_fees[tx['t_type']] / math.pow(10, 8),
                              'total': tx_amount + transaction_fees[tx['t_type']] / math.pow(10, 8),
                              'id': tx['t_id'],
                              'type': transaction_types[tx['t_type']],
                              'date': date,
                              'high': day_price['high'],
                              'low': day_price['low'],
                              'close': day_price['close'],
                              'total_high': day_price['high'] * (transaction_fees[tx['t_type']]\
                                / math.pow(10, 8) + tx_amount),
                              'total_low': day_price['low'] * (transaction_fees[tx['t_type']]\
                                / math.pow(10, 8) + tx_amount),
                              'total_close': day_price['close'] * (transaction_fees[tx['t_type']]\
                                / math.pow(10, 8) + tx_amount),
                             })

    if not txobj['data']:

        print("No transactions found for this request")
        sys.exit()

    return txobj

def tx_print(tx_obj, output_type):
    """
    Print transactions based on output type
    Args:
        tx_obj (dict) - transaction information needed
        output_type (str) - How to output (ex: csv)
    Returns:
        None
    Output:
        Formatted transactions
    """

    output = []

    if output_type == 'normal':
        fmt = '{:21} {:19} {:21} {:19} {:6} {:4} {:20} {:20} '\
            '{:22} {:12} {:12} {:12} {:12} {:12} {:12}'
    elif output_type == 'csv':
        fmt = '{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}'
    elif output_type == 'json':
        print(json.dumps(txobj))
        sys.exit()

    print(fmt.format('from',
                     'name',
                     'to',
                     'name',
                     'amount',
                     'fee',
                     'id',
                     'type',
                     'date',
                     'high',
                     'low',
                     'close',
                     'total_high',
                     'total_low',
                     'total_close'))

    for tx in tx_obj['data']:

        print(fmt.format(tx['fromAddr'],
                         tx['fromName'],
                         tx['toAddr'],
                         tx['toName'],
                         tx['amount'],
                         tx['fee'],
                         tx['id'],
                         tx['type'],
                         tx['date'],
                         float(tx['high']),
                         float(tx['low']),
                         float(tx['close']),
                         float(tx['total_high']),
                         float(tx['total_low']),
                         float(tx['total_close'])))

def get_delegates(api):
    """

    """

    payload = {'parameters': '?orderBy=rate:asc'}

    delegates = api.delegates('delegate_list', payload)

    return delegates['delegates']

def delegates_parse(delegates, output_type):
    """

    """

    output = []

    if output_type == 'normal':
        fmt = '{} {} {} {} {} {} {} {}'
    elif output_type == 'csv':
        fmt = '{},{},{},{},{},{},{},{}'
    elif output_type == 'json':
        print(json.dumps(delegates))

    print(fmt.format('username', 'producedblocks', 'missedblocks',
                     'productivity', 'rate', 'address', 'vote',
                     'approval'))

    for d in delegates:

        output.append(fmt.format(d['username'], d['producedblocks'],
                                 d['missedblocks'], d['productivity'],
                                 d['rate'], d['address'], d['vote'],
                                 d['approval']))

def get_delegate_info(api, delegate):
    """

    """

    payload = {'parameters':'/get?username={}'.format(delegate)}

    delegate_info = api.delegates('delegate_list', payload)

    return delegate_info

def get_blocks_forged(api, delegate_info):
    """

    """

    pubkey = delegate_info['delegate']['publicKey']

    # Need to get the last forged and offset by x
    parameters = '?generatorPublicKey={}&limit=1&orderBy=height:desc'.format(pubkey)
    payload = {'parameters': parameters}
    last_forged_block = api.blocks('all_blocks', payload)

    forged_count = last_forged_block['count']
    offset = forged_count - 50

    parameters = '?generatorPublicKey={}&offset={}&orderBy=height' \
                 .format(pubkey, offset)
    payload = {'parameters': parameters}
    forged_blocks = api.blocks('all_blocks', payload)

    return forged_blocks

def block_parser(forged_blocks):
    """

    """

    fmt = '{} {} {} {}'

    for block in forged_blocks['blocks']:

        new_timestamp = block['timestamp'] + 1464109200
        date = datetime.datetime.fromtimestamp(new_timestamp).strftime('%Y-%m-%d %H:%M:%S')

        total_forged = block['totalForged'] / math.pow(10, 8)

        print(date, block['height'], total_forged, int(block['totalForged']))

def embargo(fiat, exchange):
    """
    """

    # Get LSK to BTC
    url = 'https://min-api.cryptocompare.com/data/histoday?fsym=LSK&tsym=BTC&allData=true&e={}'\
            .format(exchange)
    response = requests.get(url)
    lsk_data = response.json()['Data']

    # GET BTC to USD
    url = 'https://min-api.cryptocompare.com/data/histoday?fsym=BTC&tsym={}&limit={}'\
            .format(fiat, len(lsk_data))
    response = requests.get(url)
    btc_data = response.json()['Data']

    return_data = {}

    for idx, lsk_line in enumerate(lsk_data):

        timestamp = lsk_line['time']

        s = next((item for item in btc_data if item['time'] == timestamp))

        low_price = lsk_line['low'] * s['low']
        high_price = lsk_line['high'] * s['high']
        close = lsk_line['close'] * s['close']

        date = datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d')

        return_data[date] = {
            'high': high_price,
            'low': low_price,
            'close': close
        }

    return return_data

def main(args):
    """

    """
    if 'localhost' in args.url and not args.testnet:
        api = pylisk.liskAPI(args.url, verify=False)
    elif args.testnet and args.url:
        api = pylisk.liskAPI(args.url)
    else:
        api = pylisk.liskAPI(args.url)

    url, user_nfo = get_url(args.stage, args.dpos)

    conn = get_config(args.stage, user_nfo)

    # TODO some transformation based on date given
    if args.date:
        dsplit = args.date.split('-')
        if len(dsplit) == 3:
            dyear, dmonth, dday = dsplit[0], dsplit[1], dsplit[2]
        elif len(dsplit) == 2:
            dyear, dmonth, dday = dsplit[0], dsplit[1], 1
        elif len(dsplit) == 1:
            dyear, dmonth, dday = dsplit[0], 0, 1
        dt = datetime.datetime(int(dyear), int(dmonth), int(dday), 0, 0)
        timestamp = time.mktime(dt.timetuple())

    #
    delegate = name_change(api, args.delegate)

    #
    if not delegate:
        sys.exit("Could not translate name to address")

    #
    if not args.transaction_types:
        transaction_types = [0, 1, 2, 3, 4, 5]
    else:
        transaction_types = args.transaction_types

    #
    if args.mode == 'tx_per_delegate':

        if not args.received and not args.sent:
            args.received = True
            args.sent = True

        transactions = get_transactions(conn,
                                        delegate,
                                        args.received,
                                        args.sent,
                                        timestamp)

        exchange_data = embargo(args.fiat, args.exchange)

        tx_obj = tx_parse(conn,
                          transactions,
                          args.date,
                          exchange_data,
                          transaction_types,
                          args.name)

        tx_print(tx_obj, args.output_type)


    elif args.mode == 'stats_101':

        sys.exit("nope")
        delegates = get_delegates(api)
        delegate_list = delegates_parse(delegates, args.output_type)

    elif args.mode == 'forged_blocks':

        sys.exit("nope")
        # Need to get the pubkey of the user (autoname)
        delegate_info = get_delegate_info(api, delegate)
        # Need to query the blocks forged by pubkey
        forged_blocks = get_blocks_forged(api, delegate_info)
        # there are many so need a range
        parsed_blocks = block_parser(forged_blocks)

def usage():
    """
    """

    return '''
    python3 dbtx_check.py -d slasheks --testnet --stage test --url http://localhost:7000 -o csv --date 2018-09

    Transaction Types:
    0 "Normal transaction"
    1 "2nd sig creation"
    2 "Delegate registration"
    3 "Delegate vote"
    4 "Multi-sig creation"

    '''


if __name__ == '__main__':

    parser = argparse.ArgumentParser(usage=usage())

    parser.add_argument('--dpos', dest='dpos', default='lisk',
                        choices=('lisk'),
                        action='store', help='')

    parser.add_argument('-s', '--stage', dest='stage',
                        action='store', default='test',
                        choices=('main', 'test'), help='')

    parser.add_argument('-d', '--delegate', dest='delegate', action='store',
                        help='Delegate account')

    parser.add_argument('--date', dest='date', action='store',
                        help='Date filter')

    parser.add_argument('--sent', dest='sent', action='store_true',
                        help='')

    parser.add_argument('--received', dest='received', action='store_true',
                        help='')

    parser.add_argument('--exchange', dest='exchange', default='Poloniex',
                        choices=('Poloniex', 'Bittrex', 'Yobit'),
                        action='store', help='')

    parser.add_argument('--fiat', dest='fiat', default='USD',
                        choices=('USD', 'CAD', 'EUR'),
                        action='store', help='')

    parser.add_argument('-m', '--mode', dest='mode', action='store',
                        choices=('tx_per_delegate', 'stats_101',
                                 'forged_blocks'),
                        default='tx_per_delegate', help='Output type')

    parser.add_argument('-o', '--output', dest='output_type', action='store',
                        choices=('normal', 'csv', 'json'),
                        default='normal', help='Output type')

    parser.add_argument('-u', '--url', dest='url', action='store',
                        default='http://localhost:8000',
                        help='Url to make the requests')

    parser.add_argument('--testnet', dest='testnet', action='store_true',
                        help='')

    # Filter opts
    parser.add_argument('-t', '--tx', dest='transaction_types', action='store',
                        type=int, nargs='+', help='')

    parser.add_argument('-n', '--name', dest='name', action='store',
                        help='Filter by name')

    args = parser.parse_args()

    main(args)
