#!/usr/bin/env python

import re
import sys
import json
import math
import logging
import requests
import datetime
import argparse
import pylisk

LOG = logging.getLogger(__name__)

def name_change(api, delegate):

    # if not an address then it is a name
    # try to convert to an address
    if not re.search(r'^\d+L$', delegate):

        payload = {'parameters':'?username={}'.format(delegate)}
        delegate_info = api.delegates('delegate_list',payload)

        try:
            return delegate_info['data'][0]['account']['address']
        except IndexError:
            return None

    else:

        return delegate

def static_name_mapping():
    """

    """
    try:

        with open('./name_map.json') as nmfh:

            name_map = json.load(nmfh)

            return name_map

    except IOError:

        sys.exit('Could not open name_map.json')

    pass

def paginate_transactions(api, call):
    """
    This will paginate calls with results higher than 100
    Args:
        api (class) - pylisk class for calls
        call (str)  - call that will be appended send or receive
    Returns:
        all_transactions (list) - all tx from an account
    """
    all_transactions = []
    tx_counter = 0

    # Need to make a call to get the count
    payload = {
        'parameters': '{}&limit=1'.format(call),
        'id': None
    }
    transactions = api.transactions('blocktx', payload)
    tx_count = int(transactions['meta']['count'])
    tx_iterations = tx_count / 101
    remaining_txs = tx_count % 101

    if remaining_txs:

        tx_iterations += 1

    for i in xrange(0, tx_iterations):

        req = '&offset={}'.format(tx_counter)
        new_call = call + req
        payload = {
            'parameters': new_call,
            'id': None
        }

        transactions = api.transactions('blocktx', payload)

        all_transactions += transactions['data']

        tx_counter += 101
        new_call = ''

    return all_transactions

def get_transactions(api, account, received, sent):
    """
    There is a limit on the amount of transactions, need to get all
    """
    calls = []

    if received:
        calls.append('?recipientId={}'.format(account))

    if sent:
        calls.append('?senderId={}'.format(account))

    #calls = ['?senderId={}'.format(account), '?recipientId={}'.format(account)]

    all_transactions = []

    for call in calls:

        all_transactions += paginate_transactions(api, call)

    tx_sorted = sorted(all_transactions, key=lambda k: k['timestamp'])

    return tx_sorted

def tx_parse(api, transactions, filter_date, exchange_data, tx_types, fname):
    """

    """
    txobj = {'data': []}
    name_map = static_name_mapping()

    transaction_types = {
        0: "Normal transaction",
        1: "2nd sig creation",
        2: "Delegate registration",
        3: "Delegate vote",
        4: "Multi-sig creation",
        5: "Dapp registration",
        6: "?",
        7: "Dapp deposit",
        8: "Dapp withdrawal"}

    transaction_fees = {
        0: 10000000,
        1: 500000000,
        2: 2500000000,
        3: 100000000,
        4: 500000000,
        5: 2500000000,
        6: 0,
        7: 0,
        8: 0}

    for tx in transactions:

        new_timestamp = tx['timestamp'] + 1464109200
        date = datetime.datetime.fromtimestamp(new_timestamp).strftime('%Y-%m-%d %H:%M:%S')
        day = datetime.datetime.fromtimestamp(new_timestamp).strftime('%Y-%m-%d')

        if filter_date:

            if not date.startswith(filter_date):

                continue

        # Check if it exists
        if any(x['id'] == tx['id'] for x in txobj['data']):
            continue

        tx_amount = float(int(tx['amount']) / math.pow(10, 8))

        # TODO check a static file before making an api call
        # Get the name of the sender
        try:

           from_delegate_name = name_map[tx['senderId']]

        except KeyError:

            payload = {'parameters': '?publicKey={}'.format(tx['senderPublicKey'])}
            name = api.delegates('delegate_list', payload)

            if name['data']:
                try:
                    from_delegate_name = name['data'][0]['username']
                except IndexError:
                    from_delegate_name = 'n/a'
            else:
                from_delegate_name = 'n/a'

        # Get the name of the recipient
        try:

            to_delegate_name = name_map[tx['recipientId']]

        except KeyError:

            try:

                if not tx['recipientId']:
                    raise TypeError
                payload = {'address': tx['recipientId']}
                rec_name = api.account('account', payload)
                rec_name_pubkey = rec_name['data'][0]['publicKey'] 
                to_delegate_name = rec_name ['data'][0]['delegate']['username']

            except (TypeError, KeyError):
                rec_name_pubkey = 'n/a'
                to_delegate_name = 'n/a'

        try:
            day_price = exchange_data[day]
        except KeyError:
            day_price = {'high': 0, 'low': 0, 'close': 0}

        # Filter by tx type
        if tx['type'] not in tx_types:
            continue

        # Filter by delegate name
        if fname and fname not in from_delegate_name:
            continue

        # Append to tx object what is left
        txobj['data'].append({'fromAddr': tx['senderId'],
                              'fromName': from_delegate_name,
                              'toAddr': tx['recipientId'],
                              'toName': to_delegate_name,
                              'amount': tx_amount,
                              'fee': transaction_fees[tx['type']] / math.pow(10, 8),
                              'total': tx_amount + transaction_fees[tx['type']] / math.pow(10, 8),
                              'id': tx['id'],
                              'type': transaction_types[tx['type']],
                              'date': date,
                              'high': day_price['high'],
                              'low': day_price['low'],
                              'close': day_price['close'],
                              'total_high': day_price['high'] * (transaction_fees[tx['type']]\
                                / math.pow(10, 8) + tx_amount),
                              'total_low': day_price['low'] * (transaction_fees[tx['type']]\
                                / math.pow(10, 8) + tx_amount),
                              'total_close': day_price['close'] * (transaction_fees[tx['type']]\
                                / math.pow(10, 8) + tx_amount),
                             })

    if not txobj['data']:

        print("No transactions found for this request")
        sys.exit()

    return txobj

def tx_print(tx_obj, output_type):
    """
    Print transactions based on output type
    Args:
        tx_obj (dict) - transaction information needed
        output_type (str) - How to output (ex: csv)
    Returns:
        None
    Output:
        Formatted transactions
    """

    output = []

    if output_type == 'normal':
        fmt = '{:21} {:19} {:21} {:19} {:6} {:4} {:20} {:20} '\
            '{:22} {:12} {:12} {:12} {:12} {:12} {:12}'
    elif output_type == 'csv':
        fmt = '{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}'
    elif output_type == 'json':
        print(json.dumps(txobj))
        sys.exit()

    print(fmt.format('from',
                     'name',
                     'to',
                     'name',
                     'amount',
                     'fee',
                     'id',
                     'type',
                     'date',
                     'high',
                     'low',
                     'close',
                     'total_high',
                     'total_low',
                     'total_close'))

    for tx in tx_obj['data']:

        print(fmt.format(tx['fromAddr'],
                         tx['fromName'],
                         tx['toAddr'],
                         tx['toName'],
                         tx['amount'],
                         tx['fee'],
                         tx['id'],
                         tx['type'],
                         tx['date'],
                         float(tx['high']),
                         float(tx['low']),
                         float(tx['close']),
                         float(tx['total_high']),
                         float(tx['total_low']),
                         float(tx['total_close'])))

def get_delegates(api):
    """

    """

    payload = {'parameters': '?orderBy=rate:asc'}

    delegates = api.delegates('delegate_list', payload)

    return delegates['delegates']

def delegates_parse(delegates, output_type):
    """

    """

    output = []

    if output_type == 'normal':
        fmt = '{} {} {} {} {} {} {} {}'
    elif output_type == 'csv':
        fmt = '{},{},{},{},{},{},{},{}'
    elif output_type == 'json':
        print(json.dumps(delegates))

    print(fmt.format('username', 'producedblocks', 'missedblocks',
                     'productivity', 'rate', 'address', 'vote',
                     'approval'))

    for d in delegates:

        output.append(fmt.format(d['username'], d['producedblocks'],
                                 d['missedblocks'], d['productivity'],
                                 d['rate'], d['address'], d['vote'],
                                 d['approval']))

def get_delegate_info(api, delegate):
    """

    """

    payload = {'parameters':'/get?username={}'.format(delegate)}

    delegate_info = api.delegates('delegate_list', payload)

    return delegate_info

def get_blocks_forged(api, delegate_info):
    """

    """

    pubkey = delegate_info['delegate']['publicKey']

    # Need to get the last forged and offset by x
    parameters = '?generatorPublicKey={}&limit=1&orderBy=height:desc'.format(pubkey)
    payload = {'parameters': parameters}
    last_forged_block = api.blocks('all_blocks', payload)

    forged_count = last_forged_block['count']
    offset = forged_count - 50

    parameters = '?generatorPublicKey={}&offset={}&orderBy=height' \
                 .format(pubkey, offset)
    payload = {'parameters': parameters}
    forged_blocks = api.blocks('all_blocks', payload)

    return forged_blocks

def block_parser(forged_blocks):
    """

    """

    fmt = '{} {} {} {}'

    for block in forged_blocks['blocks']:

        new_timestamp = block['timestamp'] + 1464109200
        date = datetime.datetime.fromtimestamp(new_timestamp).strftime('%Y-%m-%d %H:%M:%S')

        total_forged = block['totalForged'] / math.pow(10, 8)

        print(date, block['height'], total_forged, int(block['totalForged']))

def embargo(fiat, exchange):
    """
    """

    # Get LSK to BTC
    url = 'https://min-api.cryptocompare.com/data/histoday?fsym=LSK&tsym=BTC&allData=true&e={}'\
            .format(exchange)
    response = requests.get(url)
    lsk_data = response.json()['Data']

    # GET BTC to USD
    url = 'https://min-api.cryptocompare.com/data/histoday?fsym=BTC&tsym={}&limit={}'\
            .format(fiat, len(lsk_data))
    response = requests.get(url)
    btc_data = response.json()['Data']

    return_data = {}

    for idx, lsk_line in enumerate(lsk_data):

        timestamp = lsk_line['time']

        s = next((item for item in btc_data if item['time'] == timestamp))

        low_price = lsk_line['low'] * s['low']
        high_price = lsk_line['high'] * s['high']
        close = lsk_line['close'] * s['close']

        date = datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d')

        return_data[date] = {
            'high': high_price,
            'low': low_price,
            'close': close
        }

    return return_data

def main(args):
    """

    """
    if 'localhost' in args.url and not args.testnet:
        api = pylisk.liskAPI(args.url, verify=False)
    elif args.testnet and args.url:
        api = pylisk.liskAPI(args.url)
    else:
        api = pylisk.liskAPI(args.url)

    delegate = name_change(api, args.delegate)
    if not delegate:
        sys.exit("Could not translate name to address")

    if not args.transaction_types:
        transaction_types = [0, 1, 2, 3, 4, 5]
    else:
        transaction_types = args.transaction_types

    if args.mode == 'tx_per_delegate':

        if not args.received and not args.sent:
            args.received = True
            args.sent = True

        transactions = get_transactions(api,
                                        delegate,
                                        args.received,
                                        args.sent)

        exchange_data = embargo(args.fiat, args.exchange)

        tx_obj = tx_parse(api,
                          transactions,
                          args.date,
                          exchange_data,
                          transaction_types,
                          args.name)

        tx_print(tx_obj, args.output_type)


    elif args.mode == 'stats_101':

        delegates = get_delegates(api)

        delegate_list = delegates_parse(delegates, args.output_type)

    elif args.mode == 'forged_blocks':

        # Need to get the pubkey of the user (autoname)
        delegate_info = get_delegate_info(api, delegate)

        # Need to query the blocks forged by pubkey
        forged_blocks = get_blocks_forged(api, delegate_info)

        # there are many so need a range
        parsed_blocks = block_parser(forged_blocks)

def usage():
    """
    """

    return '''
    Filter by sent and received transactions and date
    delegate_tx_check.py -d 2581296529630509432L --date 2018 --url https://node01.lisk.io

    Filter by sent transactions and date
    delegate_tx_check.py -d 2581296529630509432L --date 2018 --url https://node01.lisk.io --sent

    Filter by received transactions and date
    delegate_tx_check.py -d 2581296529630509432L --date 2018 --url https://node01.lisk.io --received

    Filter by delegate name and date
    delegate_tx_check.py -d 2581296529630509432L --date 2018 --url https://node01.lisk.io --received --name hagie

    Filter by address
    Soon TM

    Filter by date, sent transactions and type 0 (normal)
    delegate_tx_check.py -d 2581296529630509432L --date 2017 --url https://node01.lisk.io --sent --tx 0

    Transaction Types:
    0 "Normal transaction"
    1 "2nd sig creation"
    2 "Delegate registration"
    3 "Delegate vote"
    4 "Multi-sig creation"

    '''


if __name__ == '__main__':

    parser = argparse.ArgumentParser(usage=usage())

    parser.add_argument('-d', '--delegate', dest='delegate', action='store',
                        help='Delegate account')

    parser.add_argument('--date', dest='date', action='store',
                        help='Date filter')

    parser.add_argument('--sent', dest='sent', action='store_true',
                        help='')

    parser.add_argument('--received', dest='received', action='store_true',
                        help='')

    parser.add_argument('--exchange', dest='exchange', default='Poloniex',
                        choices=('Poloniex', 'Bittrex', 'Yobit'),
                        action='store', help='')

    parser.add_argument('--fiat', dest='fiat', default='USD',
                        choices=('USD', 'CAD', 'EUR'),
                        action='store', help='')

    parser.add_argument('-m', '--mode', dest='mode', action='store',
                        choices=('tx_per_delegate', 'stats_101',
                                 'forged_blocks'),
                        default='tx_per_delegate', help='Output type')

    parser.add_argument('-o', '--output', dest='output_type', action='store',
                        choices=('normal', 'csv', 'json'),
                        default='normal', help='Output type')

    parser.add_argument('-u', '--url', dest='url', action='store',
                        default='http://localhost:8000',
                        help='Url to make the requests')

    parser.add_argument('--testnet', dest='testnet', action='store_true',
                        help='')

    # Filter opts
    parser.add_argument('-t', '--tx', dest='transaction_types', action='store',
                        type=int, nargs='+', help='')

    parser.add_argument('-n', '--name', dest='name', action='store',
                        help='Filter by name')

    args = parser.parse_args()

    main(args)
