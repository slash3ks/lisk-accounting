#!/usr/bin/env python

import sys
import math
import json
import logging
import requests

class liskAPI(object):

    def __init__(self, rturl='', verify=True):

        self.headers = {'content-type': 'application/json'}
        self.target_url = rturl
        self.verify = verify

    def get_check(self, url):
        """
        """
        r = requests.get(url, verify=self.verify)
        if r.status_code == 200:
            return json.loads(r.text)
        else:
            error = {'errcode': r.status_code}
            return error

    def account(self, rtype, payload={}):
        """
        """
        targets = {
                # Will return account by address.
                # GET /api/accounts?address=address
                'account' : '/api/accounts?address='
            }

        url = self.target_url + targets[rtype] + payload['address']
        return self.get_check(url)

    def transactions(self, rtype, payload={}):
        """
        """
        targets = {
                # Transactions list matched by provided parameters.
                # GET /api/transactions?blockId=blockId&senderId=senderId&
                # recipientId=recipientId&limit=limit&offset=offset&orderBy=field
                'blocktx' : '/api/transactions'
            }

        url = self.target_url + targets[rtype]
        url += payload['parameters']
        return self.get_check(url)

    def blocks(self,rtype,payload={}):
        """
        """
        targets = {
                # Get all blocks.
                # GET /api/blocks?generatorPublicKey=generatorPublicKey
                # &height=height&previousBlock=previousBlock&totalAmount=totalAmount
                # &totalFee=totalFee&limit=limit&offset=offset&orderBy=orderBy
                'all_blocks' : '/api/blocks'
            }

        url = self.target_url + targets[rtype]
        url += payload['parameters']
        return self.get_check(url)

    def delegates(self,rtype,payload={}):
        """
        """
        targets = {
                # Get delegates list.
                # GET /api/delegates?limit=limit&offset=offset&orderBy=orderBy
                'delegate_list' : '/api/delegates'
            }

        url = self.target_url + targets[rtype]
        url += payload['parameters']
        return self.get_check(url)


