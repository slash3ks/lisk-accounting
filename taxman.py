#!/usr/bin/env python3
import re
import sys
import json
import math
import logging
import getpass
import datetime
import argparse
import requests
try:
    import psycopg2
except ImportError:
    msg = 'Please install psycopg2. sudo apt install python3-psycopg2'
    sys.exit(msg)

def get_url(stage, dpos):
    """
    """
    url = 'http://localhost:'

    if stage == 'main' and dpos == 'lisk':
        url += '8000'
        user_nfo = {
                'user': getpass.getuser(),
                'database': 'lisk_main',
                'password': 'password'}
    elif stage == 'test' and dpos == 'lisk':
        url += '7000'
        user_nfo = {
                'user': getpass.getuser(),
                'database': 'lisk_test',
                'password': 'password'}
    elif stage == 'main' and dpos == 'shift':
        url += '9305'
        user_nfo = {
                'user': 'shift',
                'database': 'shift_db',
                'password': 'testing'}
    elif stage == 'test' and dpos == 'shift':
        url += '9405'
        user_nfo = {
                'user': 'shift_testnet',
                'database': 'shift_db_testnet',
                'password': 'testing'}
    elif stage == 'main' and dpos == 'oxy':
        url += '10000'
        user_nfo = {
                'user': 'oxycoin',
                'database': 'oxycoin_db_main',
                'password': 'password'}
    elif stage == 'test' and dpos == 'oxy':
        url += '9998'
        user_nfo = {
                'user': 'oxycoin',
                'database': 'oxycoin_db_test',
                'password': 'password'}

    return url, user_nfo

def name_change(url, delegate):
    """
    """
    # if it is an address
    LOGGER.debug("Url: {} Delegate: {}".format(url, delegate))

    if re.search(r'^\d+L$', delegate):
        response = requests.get(url + "/api/accounts?address={}".format(delegate))
        return response.json()['data'][0]['publicKey']
    elif re.search(r'^\S+$', delegate):
        if len(delegate) <= 15:
            response = requests.get(url + "/api/delegates?username={}".format(delegate))
            return response.json()['data'][0]['account']['publicKey']
    try:
        check = int(delegate, 16)
        return delegate
    except ValueError:
        sys.exit("Not a valid pubkey")

def get_config(config_path, stage, user_nfo):
    """
    """
    configuration_items = {
        'db': {
            'database': user_nfo['database'],
            'user': user_nfo['user'],
            'host': 'localhost',
            'password': user_nfo['password']
        }
    }

    if stage == 'main':
        configuration_items['db']['database'] = 'lisk_main'

    connect_str = "dbname='{database}' user='{user}' host='{host}' password='{password}'"
    conn = psycopg2.connect(connect_str.format(**configuration_items['db']))

    return conn

def execute_query(conn, sql_query, sql_query_args):

    cursor = conn.cursor()
    cursor.execute(sql_query, sql_query_args)

    column_names = [desc[0] for desc in cursor.description]

    results = []
    for row in cursor:
        try:
            new_row = [x.tobytes().decode() if isinstance(x, memoryview) else x for x in row]
            results.append(dict(zip(column_names, new_row)))
        except:
            pass

    return results

def get_blocks(conn, pubkey):
    #Full blocks by generator
    #sql_query = '''
    #    SELECT f."b_id" AS block_id, f."b_height" AS height, f."b_reward" AS reward,
    #    f."b_timestamp" AS timestamp, f."b_generatorPublicKey" AS public_key,
    #    f."b_totalFee" AS total_fees
    #    FROM blocks_list AS f
    #    WHERE f."b_generatorPublicKey" = %(pkey)s
    #    ORDER BY f."b_height" ASC
    #    '''

    #sql_query_args = {'pkey': pubkey}

    #cursor = conn.cursor()
    #cursor.execute(sql_query, sql_query_args)
    #column_names = [desc[0] for desc in cursor.description]

    #results = []
    #for row in cursor:
    #    new_row = [x.tobytes().decode() if isinstance(x, memoryview) else x for x in row]
    #    results.append(dict(zip(column_names, new_row)))

    #print(results[0])
    #{'reward': 0, 'public_key': '7beb5f1e8592022fe5272b45eeeda6a1b6923a801af6e1790933cc6a78ed95a1',
    # 'total_fees': 100000000, 'timestamp': 1659030, 'block_id': '16934049627194573838', 'height': 1787}

    #Full blocks by generator testnet
    sql_query = '''
        SELECT f."b_id" AS block_id, f."b_height" AS height, f."b_timestamp" AS timestamp
        FROM blocks_list AS f
        WHERE f."b_generatorPublicKey" = %(pkey)s
        ORDER BY f."b_height" ASC
        '''
    sql_query_args = {'pkey': pubkey}

    # Get block data
    full_blocks = execute_query(conn, sql_query, sql_query_args)
    
    # round rewards by pubkey 
    sql_query = '''
        SELECT r."timestamp", r."fees", r."reward", r."round",
        ENCODE(r."publicKey", \'hex\') AS public_key
        FROM rounds_rewards AS r
        WHERE r."publicKey" = DECODE(%(pkey)s, \'hex\')
        ORDER BY r."round" ASC
        '''
    sql_query_args = {'pkey': pubkey}

    block_rewards = execute_query(conn, sql_query, sql_query_args)

    fmt = "Block: {} Height: {} Timestamp: {} blockCalc: {} Round: {} Reward: {} Fees: {} RoundTimestamp: {}"

    results = []
    for i, b in zip(full_blocks, block_rewards):
        results.append({
            'block_id': i['block_id'],
            'height': i['height'],
            'timestamp': i['timestamp'],
            'blockcalc': math.ceil(int(i['height']) / 101),
            'round': b['round'],
            'reward': b['reward'],
            'total_fees': b['fees']})

    return {'blocks': results}

def filter_blocks(blocks, filter_date):
    """
    """

    parsed = []
    exception_counter = 0

    for block in blocks['blocks']:

        try:
            new_timestamp = int(block['timestamp']) + 1464109200
        except TypeError:
            exception_counter += 1
            new_timestamp = 0 + 1464109200

        date = datetime.datetime.fromtimestamp(new_timestamp).strftime('%Y-%m-%d-%H:%M:%S')

        if filter_date:
            if not date.startswith(filter_date):
                continue

        parsed.append(block)

    return {'blocks': parsed}


def block_output(forged_blocks):
    """

    """
    blocks_forged = []

    for block in forged_blocks['blocks']:

        try:
            new_timestamp = int(block['timestamp']) + 1464109200
        except TypeError:
            new_timestamp = 0 + 1464109200
        date = datetime.datetime.fromtimestamp(new_timestamp).strftime('%Y-%m-%d')
        total_forged = int(block['reward']) / 100000000

        blocks_forged.append({'date': date,
                              'block': block['block_id'],
                              'height': block['height'],
                              'rewards': total_forged,
                              'fees': int(block['total_fees']) / 100000000,
                              'round': block['round'],
                              'blockcalcround': block['blockcalc']})

    return blocks_forged

def embargo(fiat, exchange):
    """
    """

    # Get LSK to BTC
    url = 'https://min-api.cryptocompare.com/data/histoday?fsym=LSK&tsym=BTC&allData=true&e={}'\
            .format(exchange)
    response = requests.get(url)
    lsk_data = response.json()['Data']

    # GET BTC to USD
    url = 'https://min-api.cryptocompare.com/data/histoday?fsym=BTC&tsym={}&limit={}'\
            .format(fiat, len(lsk_data))

    response = requests.get(url)
    btc_data = response.json()['Data']
    salary_embargo = []

    for idx, lsk_line in enumerate(lsk_data):

        timestamp = lsk_line['time']

        s = next((item for item in btc_data if item['time'] == timestamp))

        low_price = lsk_line['low'] * s['low']
        high_price = lsk_line['high'] * s['high']

        date = datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d')

        salary_embargo.append({'date': date, 'high': high_price, 'low': low_price})
        #salary_embargo.append('{},{},{}\n'.format(date, high_price, low_price))

    return salary_embargo

def marriage(blocks_forged, salary_embargo, output_type):
    """
    """
    data = {}
    results = []
    for line_item in salary_embargo:
        data[line_item['date']] = {'high': line_item['high'], 'low':line_item['low']}

    for idx, line_item in enumerate(blocks_forged):
        try:
            results.append({
                'date': line_item['date'],
                'year': line_item['date'].split('-')[0],
                'month': line_item['date'].split('-')[1],
                'day': line_item['date'].split('-')[2],
                'block':line_item['block'],
                'round':line_item['round'],
                'roundcalc':line_item['blockcalcround'],
                'height': line_item['height'],
                'rewards': line_item['rewards'],
                'fees': float(line_item['fees']),
                'total': float(line_item['fees']) + float(line_item['rewards']),
                'high_price': float(data[line_item['date']]['high']),
                'low_price': float(data[line_item['date']]['low']),
                'high_sum': float(data[line_item['date']]['high']) * (float(line_item['fees']) + float(line_item['rewards'])),
                'low_sum': float(data[line_item['date']]['low']) * (float(line_item['fees']) + float(line_item['rewards']))})
        except KeyError:
            pass

    fmt = '{date} {year} {month} {day} {block} {round} {roundcalc} {height} {rewards} {fees} {total} {high_price} {low_price} {high_sum} {low_sum}'

    if output_type == 'normal':
        return output_type, {'header': fmt, 'data': [fmt.format(**x) for x in results]}
    elif output_type == 'csv':
        fmt = fmt.replace(' ',',')
        return output_type, {'header': fmt, 'data': [fmt.format(**x) for x in results]}
    elif output_type == 'json':
        return output_type, {'data': results}

def kicked_out(possession, divorce_papers):
    """
    """
    if possession == 'json':
        print(json.dumps(divorce_papers))
    else:
        print(divorce_papers['header'].replace('{','').replace('}',''))
        [print(line_item) for line_item in divorce_papers['data']]

def main(pubkey, output_type, config_path, filter_date, stage, dpos, fiat, exchange):
    """

    """

    url, user_nfo = get_url(stage, dpos)
    LOGGER.debug(url, user_nfo)

    if not pubkey:
        sys.exit("Please speficy a delegate --delegate <name|pk|addr>")

    delegate_pk = name_change(url, pubkey)
    LOGGER.debug(delegate_pk)

    conn = get_config(config_path, stage, user_nfo)

    blocks = get_blocks(conn, delegate_pk)

    parsed_blocks = filter_blocks(blocks, filter_date)

    blocks_forged = block_output(parsed_blocks)

    salary_embargo = embargo(fiat, exchange)

    possessions, divorce_papers = marriage(blocks_forged, salary_embargo, output_type)

    kicked_out(possessions, divorce_papers)

def usage():
    """
    """
    return '''

    Space separated output
    python3 taxman.py -d slasheks > output-taxman1.normal

    Comma separated output
    python3 taxman.py -d slasheks -o csv > output-taxman1.csv

    json output
    python3 taxman.py -d slasheks -o json > output-taxman1.json
    '''

if __name__ == '__main__':

    parser = argparse.ArgumentParser(usage=usage())

    parser.add_argument('-s', '--stage', dest='stage',
                        action='store', default='test',
                        choices=('main', 'test'), help='')

    parser.add_argument('-d', '--delegate', dest='delegate', action='store',
                        help='Delegate account')

    parser.add_argument('-c', '--config-path', dest='config_path',
                        action='store',
                        help='')

    parser.add_argument('--dpos', dest='dpos', default='lisk',
                        choices=('lisk', 'oxy', 'shift'),
                        action='store', help='')

    parser.add_argument('--fiat', dest='fiat', default='USD',
                        choices=('USD', 'CAD', 'EUR'),
                        action='store', help='')

    parser.add_argument('--exchange', dest='exchange', default='Poloniex',
                        choices=('Poloniex', 'Bittrex', 'Yobit'),
                        action='store', help='')

    parser.add_argument('--date', dest='date', action='store',
                        help='Date filter')

    parser.add_argument('-o', '--output', dest='output_type', action='store',
                        choices=('normal', 'csv', 'json'),
                        default='normal', help='Output type')

    parser.add_argument('-l','--log',dest='log',action='store',
                        choices=('info','debug','warning','error','critical'),
                        help='Logging facility')

    parser.add_argument('-lf','--log-file',dest='logfile', action='store',
			help='Destination log file')

    ARGS = parser.parse_args()

    LOGGER = logging.getLogger(__name__)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.getLogger("requests").setLevel(logging.WARNING)

    LEVELS = { 'debug':logging.DEBUG,
                'info':logging.INFO,
                'warning':logging.WARNING,
                'error':logging.ERROR,
                'critical':logging.CRITICAL,
                }

    if ARGS.log:

        FORMAT = '%(asctime)s %(funcName)s %(message)s '
        DATEFORMAT = '%m/%d/%Y-%I:%M:%S-%p'

        if ARGS.logfile:

            LOGGER.setLevel(LEVELS[ARGS.log])

            from logging.handlers import RotatingFileHandler

            f_handler = RotatingFileHandler(ARGS.logfile,
                                            maxBytes=10485760,
                                            backupCount=10)

            FMT = logging.Formatter(FORMAT, datefmt=DATEFORMAT)

            f_handler.setFormatter(FMT)

            LOGGER.addHandler(f_handler)

        else:

            logging.basicConfig(level=LEVELS[ARGS.log],
                                format=FORMAT,
                                datefmt=DATEFORMAT)

    else:

        logging.basicConfig(level=LEVELS["critical"])

    main(ARGS.delegate,
         ARGS.output_type,
         ARGS.config_path,
         ARGS.date,
         ARGS.stage,
         ARGS.dpos,
         ARGS.fiat,
         ARGS.exchange)
